
verbose = False


def vprint(fstr, *args):
    if not verbose:
        return

    if isinstance(fstr, str):
        print(fstr.format(*args))
    else:
        print(repr(fstr))
