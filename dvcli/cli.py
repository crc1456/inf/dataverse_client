import argparse
import os
import sys

from . import misc
from . import client




def main():
    args = parse_args()
    misc.verbose = args.verbose
    misc.vprint(args)
    args.func(args)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--token', default=None)
    parser.add_argument('-b', '--baseurl', type=str, default='https://data.goettingen-research-online.de/')
    parser.add_argument('-v', '--verbose', action='store_true')

    commands = parser.add_subparsers(dest='command')
    commands.required = True

    get_metadata_parser = commands.add_parser('get_metadata')
    get_metadata_parser.set_defaults(func=get_metadata)
    get_metadata_parser.add_argument('DOI')

    download_dataset_parser = commands.add_parser('download_dataset')
    download_dataset_parser.set_defaults(func=download_dataset)
    download_dataset_parser.add_argument('DOI')

    return parser.parse_args ()


def get_metadata(args):
    client.get_metadata(args.DOI, args.baseurl, args.token)
    

def download_dataset(args):
    client.download_dataset(args.DOI, args.baseurl, args.token)
