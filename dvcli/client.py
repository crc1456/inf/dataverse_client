#!/usr/bin/env python3

import pyDataverse as pydv
import pyDataverse.api as pydv_api
import json
import urllib
import requests
import os
import sys
import re
import io
import tarfile

from . import misc

def _download_zenodo(urlparts):
    record = urlparts.path.split('/')[-1]
    zen_r = requests.get("https://zenodo.org/api/records/{!s}".format(record))

    if zen_r.status_code != 200:
        return False

    urls = [ js['links']['self'] for js in zen_r.json()['files'] ]
    filenames = [ js['key'] for js in zen_r.json()['files'] ]

    for filename,url in zip(filenames, urls):
        misc.vprint(filename)
        r = requests.get(url)
        head,tail = os.path.split(filename)
        os.makedirs(head, exist_ok=True)
        with open(filename, 'wb') as f:
            f.write(r.content)

    return True


def _download_rdr_ucl(urlparts):
    res = requests.get(urllib.parse.urlunparse(urlparts), allow_redirects=False)
    re_match = re.search("href=\"(https://rdr.ucl.ac.uk/ndownloader/files/\w+)\"", res.content.decode('utf-8'))
    if not re_match:
        return False
    rdr_url = re_match.group(1)
    rdr_r = requests.get(rdr_url, stream=True)
    if rdr_r.status_code != 200:
        return False

    io_bytes = io.BytesIO(rdr_r.raw.read())
    tar = tarfile.open(fileobj=io_bytes, mode='r')
    for member in tar.getmembers():
        misc.vprint(member.name + ("/" if member.isdir() else ""))
    tar.extractall()
    return True



def _download_external_dataset(urlparts):
    if "doi.org" in urlparts.netloc:
        # follow doi.org links to get to the real URL
        res = requests.get(urllib.parse.urlunparse(urlparts), allow_redirects=False)
        if res.status_code in (301, 302, 308):
            # TODO: prevent infinite loop
            return _download_external_dataset(urllib.parse.urlparse(res.headers['Location']))
        else:
            return False
    elif "zenodo" in  urlparts.netloc:
        return _download_zenodo(urlparts)
    elif "rdr.ucl.ac.uk" in urlparts.netloc:
        return _download_rdr_ucl(urlparts)

    return False


def download_dataset(DOI, baseurl, token):

    api = pydv_api.NativeApi(baseurl, token)
    data_api = pydv_api.DataAccessApi(baseurl, token)

    dataset = api.get_dataset(DOI)

    files_list = dataset.json()['data']['latestVersion']['files']

    if files_list:
        for file_ in files_list:
            misc.vprint(file_)
            filename = file_["dataFile"]["filename"]
            file_id = file_["dataFile"]["id"]
            print("File name {}, id {}".format(filename, file_id))

            response = data_api.get_datafile(file_id)
            with open(filename, "wb") as f:
                f.write(response.content)
    else:
        #No files associated with it, let's check for an external link:
        for field in dataset.json()['data']['latestVersion']['metadataBlocks']['crc1456']['fields']:
            if field['typeName'].startswith('crc1456realURL'):
                extUrl = field['value']
                pu = urllib.parse.urlparse(extUrl)
                if _download_external_dataset(pu):
                    break
        else:
            raise RuntimeError('No files and not link to external dataset found')


def get_metadata(DOI, baseurl, token):

    api = pydv_api.NativeApi(baseurl, api_token=token)

    dataset = api.get_dataset(DOI)
    print(json.dumps(dataset.json(), indent=2))


