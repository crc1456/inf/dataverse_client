#!/usr/bin/env python3

from setuptools import setup

setup (name = 'dvcli',
       version = '0.1',
       packages = ['dvcli'],
       entry_points = {
           'console_scripts' : [
               'dvcli = dvcli.cli:main'
           ]
       })
